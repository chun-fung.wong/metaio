#include <octave/oct.h>

extern "C" {
  // mex.cc names both mexFunction (c) and MEXFUNCTION (Fortran)
  // but the mex file only defines one of them, so define the other
  // here just to keep the linker happy, but don't ever call it.
  void F77_FUNC(mexfunction,MEXFUNCTION)() {}
  const char *mexFunctionName = "readMeta";
} ;

DEFUN_DLD(readMeta, args, nargout,
"\
 readMeta --- Read LIGO metadata table into a MATLAB structure\n\
 By Peter Shawhan (shawhan_p@ligo.caltech.edu), April 2001\n\
 Revised by Peter Shawhan, Dec 2002, to automatically unpack spectra from BLOBs\n\
 Uses \"metaio\" parsing library by Philip Charlton\n\
 \n\
 This returns a MATLAB structure with fields which correspond to columns in\n\
 the table.  Each numeric column is converted to a vector of double-precision\n\
 real values, except that an int_8s column is stored as a vector of 8-byte\n\
 integers (Matlab class mxINT64_CLASS) to preserve full precision.\n\
 Each string or binary column is converted to a cell array containing\n\
 individual char arrays.\n\
\n\
 Usage:  a = readMeta( file [,table] [,row [,col]] )\n\
\n\
  file  is a \"LIGO lightweight\" XML file containing a Table object.\n\
  table (optional) specifies the name of the table to be read; this allows a\n\
          specific table to be read from a file containing multiple tables.  If\n\
          omitted, the first table is read. The table name is case-insensitive.\n\
  row   is a row number (counting from 1), or a vector of row numbers\n\
          (in ascending order), e.g. [2:5,10] gives rows 2 through 5 and\n\
          row 10. If omitted, or equal to 0, then all rows are included.\n\
  col   is a string containing one or more column names, separated by\n\
          commas and/or spaces.  If omitted, all columns are included.\n\
\n\
 Spectrum BLOBs (from the summ_spectrum table) are automatically unpacked into\n\
 numeric arrays, assuming the mimetype is one of the standard LIGO ones.\n\
 Note: to access the contents of a Matlab cell array, use curly braces around\n\
 the index.  For example, \"a.spectrum{1}\" gives the first spectrum array.\n\
\n\
 If readMeta(file) is called without assigning the output to a variable,\n\
 then it simply prints the column names and types and the number of rows.\n\
")
{
  octave_value_list C_mex(const octave_value_list &, const int);
  return C_mex(args, nargout);
}
